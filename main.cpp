//Jamie Maher
//CSE 20212
//Lab5
//main.cpp

#include <iostream>
#include <vector>
#include <cstring>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include "puzzle.h"

using namespace std;

int main()
{
	char choice;
	string filename;
	cout << "Would you like to play a sudoku or wordoku? (s or w)" << endl;
	cin >> choice;
	if (choice=='s')
	{
		cout << "Enter file name: " << endl;
		cin >> filename;
		puzzle<int> puz1(filename);
		puz1.play();
	}

	else if (choice=='w')
	{
		cout << "Enter file name: " << endl;
		cin >> filename;
		puzzle<char> puz2(filename);
		puz2.play();
	}

	else
		cout << "Invalid input" << endl;

	return 0;
}