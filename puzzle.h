//Jamie Maher
//CSE 20212
//Lab5
//puzzle.h

#ifndef PUZZLE_H
#define PUZZLE_H
#include <iostream>
#include <vector>
#include <cstring>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <sstream>

using namespace std;

template <typename T>
class puzzle
{
public:
	puzzle(string);
	void display();
	void input(string);
	void play();
	int solved();
	int valid();
	int horz();
	int vert();
	int box();

private:
	vector<vector<T> > puz;		// working puzzle
	vector<vector<T> > orig;	// original puzzle
	string file;				// stores file string name
	int row;					// entered row value
	int col;					// entered column value
	T value;					// entered int or char value
	int gridSize;				// 9x9
};


template<typename T>
puzzle<T>::puzzle(string n)		// non-default constructor
{
	gridSize=9;	
	file=n;
	input(file);				// sends file to input
}

template<typename T>
void puzzle<T>::input(string n)
{
	// converts file to vector of vectors
	string line;
	ifstream myFile;
	myFile.open(file.c_str(), ifstream::in);
	if(myFile.is_open())
	{
		while (getline(myFile, line))
		{
			vector<T> thisLine;
			T n;
			istringstream iss(line);
			while (iss >> n)
			{
				thisLine.push_back(n);
			}
			// sends the line to both the working and original
			puz.push_back(thisLine);
			orig.push_back(thisLine);
		}
		myFile.close();
	}
	else
		cout << "File could not load properly" << endl;
}

template<typename T>
void puzzle<T>::display()
{
	int vertCount=0, horzCount=0;
	cout << " -----------------------" << endl;
	for (int i=0; i<gridSize; i++)
	{
		cout << "| ";
		for (int j=0; j<gridSize; j++)
		{
			cout << puz[i][j] << " ";
			if (horzCount==2)
			{
				cout << "| ";
				horzCount=0;
			}
			else
				horzCount++;
		}
		cout << endl;
		if (vertCount==2)
		{
			cout << " -----------------------" << endl;
			vertCount=0;
		}
		else
			vertCount++;
	}

}

template<typename T>
void puzzle<T>::play()
{
	display();
	while (!solved())
	{
		cout << "Row: " << endl;
		cin >> row;
		row=row-1;
		cout << "Column: " << endl;
		cin >> col;
		col=col-1;
		cout << "Value: " << endl;
		cin >> value;

		if (valid()) // checks for legal moves
		{
			system("clear");
			puz[row][col]=value;
			display();
		}
		else
		{
			cout << "Please try again" << endl;
			display();
		}
	}

	cout << "Congratulations, you solved the puzzle!" << endl;
}


template<typename T>
int puzzle<T>::solved()
{
	for (int i=0; i<gridSize; i++)
	{
		for (int j=0; j<gridSize; j++)
		{
			// if there is a 0 somewhere still, not solved
			if (puz[i][j]==0 || puz[i][j]=='0') 
				return 0;
		}
	}
	return 1;
}

template<typename T>
int puzzle<T>::valid()
{
	// if the entered position was an original number
	if (orig[row][col]!=0 && orig[row][col]!='0')
	{
		system("clear");
		cout << "Can't modify an original value" << endl;
		return 0;
	}
	if (horz() && vert() && box())  // checks for duplicates
		return 1;
	else
	{
		system("clear");
		return 0;
	}
}

template<typename T>
int puzzle<T>::horz()
{
	// checks the row for duplicate
	// skips current position so it can be rewritten
	for (int i=0; i<gridSize; i++)
	{
		if (puz[row][i]==value && i!=col)
			return 0;
	}
	return 1;
}

template<typename T>
int puzzle<T>::vert()
{
	// checks column for duplicate
	// skips current position so it can be overwritten
	for (int i=0; i<gridSize; i++)
	{
		if (puz[i][col]==value && i!=row)
			return 0;
	}
	return 1;
}

template<typename T>
int puzzle<T>::box()
{
	// uses modulus function to adjust for the 
	// upper left corner of the box
	// skips current positions so it can be rewritten
	for (int i=row-(row%3); i<(row-(row%3))+2; i++)
	{
		for (int j=col-(col%3); j<(col-(col%3))+2; j++)
		{
			if (puz[i][j]==value && i!=row && j!=col)
				return 0;
		}
	}
	return 1;
}

#endif